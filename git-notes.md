# Git notes
> download the new changes from the remote repository (on branch e.g.: master):

`git pull`

> create copy branch from your current (local) repository branch name is
> important and there are different rules on different projects. E.g.: on 
> hotels we started every branch name with 'feature/' or 'bugfix/' prefix then
> the name of your task from your project management tool. E.g.: my team was
> CCAT and our every task started with CCAT-NUMBER. It is important because
> hooks can be configured to git (github or bitbucket) which can trigger
> automatic builds and deploys. On hotels feature branches were built
> automatically and master branch built and deployed on test environment
> automatically. (remote modifications not local)

> my test branch is: feature/PR-1-review-items
 
`git checkout -b feature/JIRA-NUMBER-any-text-without-whitespaces`

> This create a new feature branch and change current branch to it. (without
> pull your last modifications do not appear within it.)

> With git status can check your local modifications. There are 3 status:
> - untracked: git does not handle this files. --> red
> - unstaged: there are modifications, but not staged --> red
> - staged: these files will be saved next commit --> green

`git status`

> If you make changes you can stage your modifications and new files with 
> `git add` where you can use regex and path as well:
> - All modifications within the directory: `git add .`
> - Modifications within package: 
> 
> `git add api-exception-handler/src/main/java/com/practice/exceptions/*`

`git add pom.xml`

> If you staged your modifications you can finalize them with commit.
> There is a lot of options for them:
> - `git commit` opens predefined text editor (vim is the default) where you 
> can add a message.
> - `git commit -m JIRA-NUMBER: commit message` create the commit with the 
> argument message.
> - `git commit --amend` modify last commit. You can modify the message or can
> add new modifications with `git add`
>
> It is a good practice (or requirement of the project) to start the commit
> message with ticket number of the task. After you merged your PR into master
> you can't distinguish your tickets on the git history unless you follow this
> practice. The other reason for that there can be bind hooks on your git
> message as well.
> Beware of commit messages with "refactor" or "fix PR reviews", because it has
> no sense. You should use a short but expressing messages, e.g.: 
> _extract class: CurrencyIdProvider from CurrencyService_ or 
> _introduce new API endpoint: api/users/register_ or 
> _add unit tests for CurrencyIdProvider_

`git commit -m "PR-1: add review items to parent pom.xml"`

> If you finished your changes or you have to share your work with your 
> colleagues or just this is the end of the day you should `push` your changes.
> If this is the first time you should create a new remote repository branch, 
> but Git bash helps you: e.g.:
>
> `git push --set-upstream origin feature/PR-1-review-items`

`git push`

## Other useful commands:

> You can delete unused branches:

`git branch -d feature/PR-1-unused-branch`

> You can reset your staged commits:
> - Soft (make your modification unstaged) 
>
> `git reset api-exception-handler/src/main/java/com/practice/exceptions/*`
> - Hard reset (reverts your changes) 
>
> `git reset --hard api-exception-handler/src/main/java/com/practice/exceptions/*`
>
> I think it is a good practice to reset hard via IntelliJ, because it is more
> apparent. To do it: right click on file: Git\Rollback

> You can save your work into stash and pop it (It is a stash so the last saved
> files come first back)
> - to save: `git stash`
> - to list: `git stash list`
> - to get back: `git apply`
> - to delete last: `git stash drop`

> You can change your current branch into other if your workspace is clean 
> (everything is committed or stashed)
> - to list your local branches: `git branch`
> - to change your branch: `git checkout branchname`
> - to change back `git checkout -`

> When somebody update the master or any other branch and your branch needs
> this modification:
> - `git checkout master` -> or other branch
> - `git pull`
> - `git checkout -` -> it is your working branch
> - `git merge master` -> this will save the new modifications into your branch from master
>
> If there is a conflict you should open intelliJ right click on project and
> git\resolve conflicts can help you
> 
> After the modifications you should commit the modifications. This case the
> default commit message is ok, you don't need to override it.