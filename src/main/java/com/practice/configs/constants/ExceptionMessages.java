package com.practice.configs.constants;

public final class ExceptionMessages {

	public static final String FAILED_TO_PROCESS_THE_RESPONSE = "Failed to process the response";
	public static final String ENTITY_NOT_FOUND = "entity not found";
	public static final String FAILED_TO_UPDATE_THE_ENTITY = "failed to update the entity";

	private ExceptionMessages() {}

}
