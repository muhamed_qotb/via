package com.practice.configs.constants;

public final class Patterns {

	public static final String NAME_PATTERN = "[A-Za-z]+";
	public static final String USER_NAME_PATTERN = "[A-Za-z_]+";
	public static final String PHONE_NUMBER_PATTERN = "^(06)[0-9]{1,2}[0-9]{6}$";
	public static final String CURRENCY_CODE_PATTERN = "[A-Za-z]{3}";

	private Patterns() {}

}
