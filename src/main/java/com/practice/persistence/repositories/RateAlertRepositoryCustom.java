package com.practice.persistence.repositories;

import java.util.List;

public interface RateAlertRepositoryCustom {

	List<String> getBases();

}
