package com.practice.helpers;

public final class IDs {

    public static final long EXISTING_ID = 5L;
    public static final long NON_EXISTING_ID = 404L;
    public static final int VALID_AGE = 30;
    public static final int INVALID_AGE = 20;

    private IDs() {}

}
