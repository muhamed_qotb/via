INSERT INTO employee (id, name, username, email, phone_number, age) VALUES 
	(1, 'Charlotte', 'Charlotte_Jack', 'Charlotte_Jack@test.com', '061123456', 25),
	(2, 'Thomas', 'Thomas_Joseph', 'Thomas_Joseph@test.com', '062123456', 26),
	(3, 'Olivia', 'Olivia_Matthew', 'Olivia_Matthew@test.com', '063123456', 27),
	(4, 'Luke', 'Luke_Samuel', 'Luke_Samuel@test.com', '064123456', 28),
	(5, 'Hannah', 'Hannah_Joshua', 'Hannah_Joshua@test.com', '065123456', 29);
	
INSERT INTO rate_alert (id, base, email) VALUES 
	(1, 'HUF', 'Charlotte_Jack@test.com');
	

