FROM maven:3.6.3-openjdk-11-slim
COPY target/employee-api-1.0.jar employee-api-1.0.jar
ENTRYPOINT ["java", "-jar", "employee-api-1.0.jar"]