CREATE TABLE employee (
    id serial NOT NULL,
    name character varying(250) NOT NULL,
    username character varying(250) NOT NULL,
    email character varying(250) NOT NULL,
    phone_number character varying(10) NOT NULL,
    age integer NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT employee_unique_username UNIQUE (username),
    CONSTRAINT employee_unique_email UNIQUE (email),
    CONSTRAINT employee_unique_phone_number UNIQUE (phone_number)
);

CREATE TABLE rate_alert (
  id serial,
  base CHAR(3) NOT NULL,
  email VARCHAR(250) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT rate_alert_unique_email UNIQUE (email)
);
